# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score | Completed |
|:----------------------------------------------------------------------------------------------:|:-----:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  | T |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  | T |
|         All things in your game should have correct physical properties and behaviors.         |  15%  | T |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  | T |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  | T |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  | T |
| Appearance (subjective)                                                                        |  10%  | T |
| Other creative features in your game (describe on README.md)                                   |  10%  | T |

## Report
完成项目的附加说明：
1.方向键移动松鼠，Up键跳跃，原地跳跃后从当前台阶跳下（穿过此台阶).<br>
2.安全平台每次跳上后生命值加一点；松鼠被尖刺扎到会弹回，生命值-5，画面闪红，并有出血音效；被电到，生命值-5（只扣一次），画面闪白，震动，有电击声效;传送带使松鼠向平台两端自动移动；粘液池上松鼠移动速度减慢.<br>
3.排行榜只显示分数最高的八个记录，但所有的记录都有存储于数据库.<br>
4.游戏中不会看到排行榜，游戏结束后按Q可显示，用户可以输入自己的名字，上传自己的成绩（但不强制用户输入自己的成绩），之后按enter键重新开始.<br>
5.每次游戏后用户只能上传一次成绩，避免恶意刷屏。Restart游戏并结束后可再次上传成绩。由于可能存在重名现象，因此排行榜中允许存在同一个名字的多个成绩.<br>
6.gitlab网址：https://x1062111.gitlab.io/Assignment_02/      firebase网址：https://as2-downstairs.firebaseapp.com/.<br>
## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
