var game = new Phaser.Game(550, 600, Phaser.AUTO, 'canvas',
    { preload: preload, create: create, update: update });

    var player;
    //var keyboard;
    
    var platforms = [];
    var beginPlatform;
    var leftWall;
    var rightWall;
    var ceiling;
    var cursor;
    var keyboard;
    var lifeLable='';
    var score='';
    var endTip='';
    var endTip2='';
    var menu='';
    var floors = 0;
    var lastTime= 0;

    var status= 'stop' ;
   
    var elecSound;
    var bloodSound;    
    var flag=false;
    var rankBoard;
    var scoreUpdated=false;
   
    function preload () {

        game.load.image('lwall', 'assets/lwall.png');
        game.load.image('rwall', 'assets/rwall.png');
        
        game.load.image('safebrick', 'assets/safebrick.png');
        game.load.image('spike', 'assets/spike.png');
        game.load.image('poison', 'assets/poison.png');


        game.load.spritesheet('belt', 'assets/belt.png', 193, 32);
        game.load.spritesheet('nbelt', 'assets/belt.png', 193, 32);
        game.load.spritesheet('electric', 'assets/electric.png', 106, 36);
        game.load.spritesheet('player', 'assets/chipwalk1.png', 40, 45);
        game.load.image('ceiling', 'assets/ceiling.png');
        ///
        game.load.audio('electric', 'assets/elec.ogg', 'assets/elec.mp3');
        game.load.audio('blood', 'assets/blood.ogg', 'assets/blood.mp3');
        
        var style = {fill: 'white', fontSize: '20px'}
        lifeLable = game.add.text(0, 0, '', style);
        score = game.add.text(450,0, '', style);
        endTip = game.add.text(140, 200, 'Press Enter to Restart', style);
        endTip.visible = false;
        endTip2 =  game.add.text(140, 250, 'Press Q to Record Your Score', style);
        endTip2.visible = false;
        menu = game.add.text(120, 200, 'Ready to Start?Press Enter!', style);
        menu.visible = true;
    }

    function create(){
        
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'q': Phaser.Keyboard.Q
        });
        //设置边界
        leftWall = game.add.sprite(0,22, 'lwall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(521, 22, 'rwall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        ceiling = game.add.sprite(0, 22, 'ceiling');
        game.physics.arcade.enable(ceiling);
        ceiling.body.immovable = true;
        //设置player
        cursor = game.input.keyboard.createCursorKeys();

        beginPlatform = game.add.sprite(game.width/2-70,400,'safebrick');
        player = game.add.sprite(game.width/2-30,game.height/2, 'player');
        game.physics.arcade.enable(beginPlatform);
        beginPlatform.body.immovable = true;
        platforms.push(beginPlatform);
        
        player.facingLeft = false;

        player.animations.add('rightwalk',[1,2,5,6],8,true);
        player.animations.add('leftwalk',[3,4,7,8],8,true);
        player.animations.add('rightjump',[1,0],8,false);
        player.animations.add('leftjump',[3,9],8,false);
        player.animations.add('electricHurt',[0,1,5,9],8,true);
        
        game.physics.arcade.enable(player);

        elecSound = game.add.audio('electric');
        bloodSound = game.add.audio('blood');
        
        // Add vertical gravity to the player
        player.body.gravity.y = 500;
        player.life=20;
        player.touchOn = undefined;
        //显示生命值和楼层数
    }
   
    function update() {
        game.physics.arcade.collide(player, leftWall);
        game.physics.arcade.collide(player, rightWall);
        game.physics.arcade.collide(player, platforms,effect);
        game.physics.arcade.collide(player, ceiling,topEffect);
        
        lifeLable.setText('life:' + player.life);
        score.setText('floor:' + floors);
        if (!player.inWorld||player.life<=0) {
            endTip.visible = true;
            endTip2.visible = true;
            platforms.forEach(function(s) {s.destroy()});
            player.life=0;
            platforms = [];
            status = 'stop';  
            flag=true; 
            player.kill();
            
        }
        if(keyboard.enter.isDown&&menu.visible){
            menu.visible = false;
            status = 'running';
        }
        if(keyboard.enter.isDown&&flag==true) 
        {
            create();
            floors=0;
            endTip.visible = false;
            endTip2.visible = false;
            status = 'running';
            flag = false;
            scoreUpdated=false;
            rankBoard.style.display="none"; 
        }
        if(keyboard.q.isDown&&flag==true) 
        {
            
            InputName();
             
        }
        if(status != 'running') return;
        movePlayer();
        updatePlatforms();
        createPlatforms();
        //console.log(platforms.length);
    }
   
    
    function movePlayer() {
        if (cursor.left.isDown) {
            player.body.velocity.x = -200;
            player.facingLeft = true;
            player.animations.play('leftwalk'); 
            ///
        }
        else if (cursor.right.isDown) { 
            player.body.velocity.x = 200;
            player.facingLeft = false;
            player.animations.play('rightwalk'); 
            ///
        }    

        else if (cursor.up.isDown) { 
            if(player.body.touching.down){
                // Move the player upward (jump)
                if(player.facingLeft) {
                    player.animations.play('leftjump'); 
                }else {
                    player.animations.play('rightjump'); 
                }
                player.body.velocity.y = -350;
            }
        }  
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            player.body.velocity.x = 0;
        
            if(player.facingLeft) {
                // Change player frame to 3 (Facing left)
                player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                player.frame = 1;
            }
            // Stop the animation
            player.animations.stop();
        }    
    }
    
    function updatePlatforms () {
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 3;
            if(platform.body.position.y <= 50) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    }
   function createPlatforms() {
        if(game.time.now > lastTime + 1000) {
           lastTime = game.time.now;

            var newPlatform;
            var x = Math.random()*(492 - 193) + 29;
            var y = 600;
            var n = Math.random() * 10;

            if(n < 3) {
                newPlatform = game.add.sprite(x, y, 'safebrick');
              
            } else if (n < 5) {
                newPlatform = game.add.sprite(x, y, 'spike');
                game.physics.arcade.enable(newPlatform);
                newPlatform.body.setSize(131, 24, 0, 20);     
            } else if (n < 6) {
                newPlatform = game.add.sprite(x, y, 'belt');
                newPlatform.animations.add('scroll', [0, 1], 16, true);
                newPlatform.play('scroll');
            }else if (n < 7) {
                newPlatform = game.add.sprite(x, y, 'nbelt');
                newPlatform.animations.add('nscroll', [1, 0], 16, true);
                newPlatform.play('nscroll');

            } else if (n < 8) {
                newPlatform = game.add.sprite(x, y, 'electric');
                newPlatform.animations.add('power', [0, 1, 2, 3], 10,true);    
                newPlatform.play('power');
                game.physics.arcade.enable(newPlatform);
                newPlatform.body.setSize(106, 18, 0, 18);
            } else {
                newPlatform = game.add.sprite(x, y, 'poison');
                
            }

            game.physics.arcade.enable(newPlatform);
            newPlatform.body.immovable = true;
            platforms.push(newPlatform);

            floors += 1;
        }
    }
    function topEffect (player, Platform) { 
        player.life -= 5;
        game.camera.flash(0xff0000, 300);
        bloodSound.play();
    
    };
    function effect(player, platform) {
        if(platform.key == 'safebrick') {
            safeEffect(player, platform);
        }
        if(platform.key == 'spike') {

            spikeEffect(player, platform);
        }
        if(platform.key == 'poison') {
            poisonEffect(player, platform);
        }
        if(platform.key == 'belt') {
            leftSlideEffect(player, platform);
        }
        if(platform.key == 'nbelt') {
            rightSlideEffect(player, platform);
        }
        if(platform.key == 'electric') {
            electricEffect(player, platform);
        };

        function safeEffect (player, Platform) {
            if (player.touchOn !== Platform) {
                if(player.life < 20){
                    player.life += 1;
                }
                player.touchOn = Platform;
            }
            
        }
        function spikeEffect (player, Platform) { 
                player.life -= 5;
                player.body.velocity.y = -480;
                game.camera.flash(0xff0000, 300);
                bloodSound.play();
            
        };
        function rightSlideEffect (player, platform){
            player.body.x += 2;
           
        };
        function  leftSlideEffect(player, platform){
            player.body.x -= 2;       
        };
        function electricEffect(player, Platform){
            if (player.touchOn != Platform) {
                player.life -= 5;
                player.touchOn = Platform;
                elecSound.play();
                game.camera.shake(0.02, 300);
                game.camera.flash(0xffffff, 300);
                
            }
        }
        function  poisonEffect(player, platform) {
            if (cursor.left.isDown) {
                player.body.x += 2.5;
                player.facingLeft = true;
            }
            else if (cursor.right.isDown) { 
                player.body.x -= 2.5;
                player.facingLeft = false;
          
            }    
        }
    }
    function InputName(){
        rankBoard=document.getElementById("rankBoard")
        rankBoard.style.display="block";  
    }
    function init(){
        var userInfoRef= firebase.database().ref("players"); 
        var submit_btn = document.getElementById("submit");
        var name = document.getElementById("NameInput");
        submit_btn.addEventListener('click', function () {
            if (name.value != ""&&scoreUpdated==false) {
                firebase.database().ref("players").push();
                var newKey= firebase.database().ref("players").push().key;
                firebase.database().ref("players/"+newKey).set({
                    "Name":name.value,
                    "score":floors,
                });
                alert("Update Success!");  
                scoreUpdated=true;   
            }
            else if (scoreUpdated==true) { 
                alert("You Have Already Updated the Score!");               
            }
            else{
                alert("Please Input Your Name !");     
            }               
        });
        var scoresRef =firebase.database().ref("players");
        scoresRef.orderByChild("score").limitToLast(8).on("value", function(snapshot) {
        var num=0;
        snapshot.forEach(function(data) {
            num++;
        });
        var i=num;
        snapshot.forEach(function(data) {
            var scorei = document.getElementById("score"+i);
            var namei = document.getElementById("name"+i);
            var childData = data.val();
            scorei.innerHTML=childData.score;
            namei.innerHTML=childData.Name;
            i--;
        });
});
    }
window.onload = function () {
    init();   
};